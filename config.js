/* eslint-disable */

if ( process.env.NODE_ENV !== 'production' ) {
   if (module.hot) {
     module.hot.accept()
   }
}

import './assets/styles/style';
import MegaHeader from './assets/scripts/script';

import PrismaConnect from './pmc-user/assets/scripts/script'

new MegaHeader()
new PrismaConnect()

import 'template/shared/fonts/config';

import './logo/config'
import './brand-nav/config'
import './pmc-user/config'
import './ad-button/config'
import './search/config'
import './navigation/config'
import './mag-subscription/config'
import './social-nav/config'
import './pmc-user/config'
import './bookmark-btn/config'

import './sticky-bar/config'

//import Navigation from 'template/shared/mega-header/navigation/config';
//import Search from 'template/shared/mega-header/search/config';

