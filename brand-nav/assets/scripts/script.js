const logoSections = Array.from(document.getElementsByClassName('logo-section'))
let logoSection

for (logoSection of logoSections) {
  const close = logoSection.getElementsByClassName('close')[0]

  const switcherVisible = function (e) {
    //console.log(e.target)
    const brandSwitcher = this.getElementsByClassName('brand-switcher')[0]

    brandSwitcher.classList.add('visible')
  }

  const switcherHidden = function (e) {
    const brandSwitcher = this.getElementsByClassName('brand-switcher')[0]

    setTimeout(function(){
      brandSwitcher.classList.remove('visible')
    }, 500)
  }

  const clickClose = function (e) {
    const brandSwitcher = this.parentNode.parentNode

    brandSwitcher.classList.remove('visible')
  }

  logoSection.addEventListener('mouseover', switcherVisible)
  logoSection.addEventListener('touchend', switcherVisible)
  logoSection.addEventListener('mouseleave', switcherHidden)
  close.addEventListener('click', clickClose)
  close.addEventListener('touchstart', clickClose)

  //fallback for background-blend-mode
  const addBackgroundFallback = () =>{
    const brandList = logoSection.getElementsByClassName('brand-list')[0]

    if(typeof window.getComputedStyle(document.body).backgroundBlendMode == 'undefined') {
      brandList.className += " no-background-blend-mode";
    }
  }

  addBackgroundFallback()
}

