/* global pmcstarter */

export default class PrismaConnect {
  constructor () {
    this.name = 'PrismaConnect'
    this.init()
  }

  init () {
    console.log('qqqqqqqqqqq')
    this.initPrismaConnect()
  }

  imageResizerEncodeUrl (url) {
    return encodeURIComponent(url)
      .replace(/!/g, '%21')
      .replace(/'/g, '%27')
      .replace(/\(/g, '%28')
      .replace(/\)/g, '%29')
      .replace(/\*/g, '%2A')
      .replace(/%20/g, '+')
      .replace(/\./g, '%2E')
      .replace(/%/g, '.')
  }

  /**
   *
   * @param {string} host
   * @param {string} op - operation fit, pad, or scale
   * @param {string} url
   * @param {array} args - arguments for resizer
   * @param {string} filename
   * @param {string} format - ex: jpg/png/gif (no dot)
   * @returns {string}
   */
  imageResizerResizeUrl (host, op, url, args, filename, format) {
    return host + '/' + op + '/' + this.imageResizerEncodeUrl(url) + '/' + args.join('/') + '/' + format + '/' + filename
  }

  initPrismaConnect () {
    pmcstarter.init({
      'domain': 'femmeactuelle.fr'
    })

    // Init all the buttons
    pmcstarter.initAll()

    $(pmcstarter.isConnected() && pmcstarter.getProfileCookie() ? '.pmc-connected' : '.pmc-not-connected').removeClass('hidden')

    if (pmcstarter.isConnected() && pmcstarter.getProfileCookie()) {
      if (pmcstarter.getProfileAvatar() && typeof this.imageResizerResizeUrl === 'function') {
        var host = globalConfigs.pmd_image_resizer.client_host
        var src = pmcstarter.getProfileAvatar()
        var filename = src.substr(src.lastIndexOf('/') + 1)
        if (!filename || !filename.match(/.(jpg|jpeg|png|gif)$/i)) { filename = filename + '.jpg' }

        var format = filename.substr(filename.lastIndexOf('.') + 1)
        var resizerArgs = ['35x35', 'quality', '100', 'crop-from', 'top']
        var avatar = this.imageResizerResizeUrl(host, 'fit', src, resizerArgs, filename, format)
        $('[data-user-avatar]').attr('src', avatar)
      }

      if (pmcstarter.getProfileId() == $('[data-owner-uid]').attr('data-owner-uid')) {
        $('[data-private]').removeClass('hide')
      }

      if (!this.isWrapper) {
        if (this.gtm) {
          if (!this.gtm.hasEvent('userLoggedIn')) {
            this.gtm.extendsDataLayer('userHash', this.gtm.hashEmail(pmcstarter.getProfileEmail()))
            this.gtm.extendsDataLayer('event', 'userLoggedIn')
          }
        }
      }
    }

    if (!this.isWrapper && typeof GoogleAnalyticsEvent !== 'undefined') {
      this.gaEvent.addEvent('send', 'event', 'User', 'Page vue connectée ', {
        'nonInteraction': 1,
        'dimension11': pmcstarter.isConnected() ? 'yes' : 'no'
      })
    }
  }
}
