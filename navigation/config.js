/* eslint-disable */

/*
 * shared/header/navigation
 */
import './assets/styles/style';
import NavMenuActions from './assets/scripts/script'

const navRzr = document.querySelector('[data-navresizer]')

const navMenu = new NavMenuActions(navRzr)

window.addEventListener("resize", resizeThrottler, false);

let resizeTimeout
function resizeThrottler() {
  navMenu.prepareForResize()
  // ignore resize events as long as a resizeHandler execution is in the queue
  if ( !resizeTimeout ) {
    resizeTimeout = setTimeout(function() {
      resizeTimeout = null;
      resizeHandler();
    }, 66);
  }
}

function resizeHandler() {
  navMenu.init()
}

