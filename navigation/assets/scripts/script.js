// Test for multiple import dependency
import jQuery from 'vendor/jquery/dist/jquery'

jQuery(document).ready(function () {
  console.log('Script Shared navigation')
})

/* global $ */

export default class NavMenuActions {
  constructor (elt) {
    this.version = '0.1'
    this.id = Date.now()

    this.$container = $(elt)
    this.$mainMenu = this.$container.find('[data-header-menu]')
    this.$searchItem = this.$container.find('[data-search]')
    this.$extendMenu = this.$container.find('[data-navresizer-container]')
    this.$extendMenuList = this.$extendMenu.find('[data-navresizer-ul]')

    this.init()
  }

  init () {
    if (this.$mainMenu.hasClass('ov-h')){
      this.$mainMenu.removeClass('ov-h')
    }

    if (window.matchMedia('(min-width: 991px)').matches) {
      const self = this
      if (this.$extendMenu.length && window.matchMedia('(min-width: 991px)').matches) {
        $(window).on('resize.navResizr-' + this.id, function () {
          if ($(window).width() >= 991) {
            clearTimeout(this.id)
            this.id = setTimeout(function () {
              self.refreshMenuItem()
            }, 100)
          } else {
            return false
          }
        })

        setTimeout(function () {
          $(window).resize()
        }, 200)
      }
      this.addEventDropdownMenu()
    }

    if (window.matchMedia('(max-width: 990px)').matches) {
      this.scrollToCurrent()
    }
  }

  prepareForResize () {
    if (!this.$mainMenu.hasClass('ov-h')){
      this.$mainMenu.addClass('ov-h')
    }
  }

  refreshSubmenuPosition (item) {
    var submenu = $(item).find('.dropdown-menu')
    var itemMenuPosition = $(item).position()
    var container = $(item).parents('.container')
    var submenuRightPosition = itemMenuPosition.left + submenu.outerWidth(true) + 5 // +5 fix last node
    submenu.css('right', 'auto')
    if (submenuRightPosition >= container.width()) {
      submenu.css('right', 0)
    }
  }

  moveToExtendedMenu ($item) {
    if ($item.find('.dropdown-toggle').get(0)) {
      $item
        .removeClass('dropdown')

      $item.find('.dropdown-toggle')
        .removeClass('dropdown-toggle')
        .addClass('dropdown-toggle-old')
    }

    this.$extendMenuList.eq(0).prepend($item)
  }

  moveToMainMenu ($item) {
    if ($item.find('.dropdown-toggle-old').get(0)) {
      $item
        .addClass('dropdown')

      $item.find('.dropdown-toggle-old')
        .removeClass('dropdown-toggle-old')
        .addClass('dropdown-toggle')
    }

    $item.insertBefore(this.$extendMenu)
  }

  refreshMenuItem () {
    var searchWidth = 0
    var mainMenuItems = this.$extendMenu.prevAll('li').get().reverse()
    var $extendedMenuItems = this.$extendMenuList.find('> li')

    const self = this

    /*if (this.$searchItem.length) {
      searchWidth = this.$searchItem.width()
    }*/

    // put in main menu
    $extendedMenuItems.each(function (i, item) {

      console.warn('[data-navresizer]' + self.$container.width(), '[data-header-menu]' + self.$mainMenu.width())
      if (self.$container.width() - self.$mainMenu.width() > self.$extendMenuList.width()) {
        self.moveToMainMenu($(item))
      } else {
        return false
      }
    })

    // put in extend menu
    $(mainMenuItems).each(function (i, item) {
      if (
        ($(item).prev().get(0) && $(item).prev().offset().left > $(item).offset().left) || // next item is not on the same line
        ($(item).position().left + $(item).outerWidth(true) > self.$container.outerWidth(true) - searchWidth) // right offset of the element is bigger than right offset of the menu
      ) {
        var $items = $($(item).prev().nextUntil('[data-navresizer-container]').get().reverse())

        $items.each(function (i, item) {
          self.moveToExtendedMenu($(item))
        })
        return false
      }
    })

    if (this.$extendMenuList.find('> li').length) {
      self.$extendMenu.removeClass('hidden')
    } else {
      self.$extendMenu.addClass('hidden')
    }
  }

  addEventDropdownMenu () {
    const toggle = this.$container.find('a.dropdown-toggle')
    const self = this

    toggle.on('click', function (event) {
      event.stopImmediatePropagation()
    })

    this.$container
      .on('mouseenter', '.dropdown:not(.more)', function (event) {
        event.preventDefault()

        $(event.target).not(this).stop().removeClass('open')
        $(this).stop().addClass('open')

        self.refreshSubmenuPosition(this)
      })
      .on('mouseleave', '.dropdown:not(.more)', function (event) {
        event.preventDefault()
        $(this).stop().removeClass('open')
      })
  }

  scrollToCurrent () {
    const current = this.$mainMenu.find('.current')[0]

    if (current) {
      current.scrollIntoView({block: 'center'})
    }
  }
}
