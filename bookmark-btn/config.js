import './assets/styles/style'
import './assets/scripts/script'

import '../tooltip/config'

if ( process.env.NODE_ENV === 'development') {
  if (module.hot) {
    module.hot.accept()
  } else {
    console.log('no HMR')
  }
} else {
  console.log(process.env.NODE_ENV)
}


