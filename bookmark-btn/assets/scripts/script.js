const buttonWrappers = Array.from(document.getElementsByClassName('bookmark-btn'))
let buttonWrapper

for (buttonWrapper of buttonWrappers) {
  const button = buttonWrapper.getElementsByTagName('button')[0]
  const tooltip = buttonWrapper.getElementsByClassName('tooltip')[0]

  const likeText = tooltip.getElementsByClassName('like-text')[0]

  const state = button.dataset.bookmarkStatus === 'true'

  const showBookmarkMessage = function (e) {
    if (state) {
      likeText.innerHTML = 'Enregistré'
      //console.warn(likeText)
    } else {
      likeText.innerHTML = 'Enregistrer'
      //console.warn(likeText)
    }
  }

  showBookmarkMessage()
  button.addEventListener('click', showBookmarkMessage)
}
