if ( process.env.NODE_ENV === 'development' ) {
  if (module.hot) {
    module.hot.accept()
  }
}

import './assets/styles/style'
import './assets/scripts/script'
