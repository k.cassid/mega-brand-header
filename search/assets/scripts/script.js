/*
 * search
 */
const toggle = document.getElementsByClassName('search-toggle')[0]
const open = document.getElementsByClassName('open-search')[0]
const close = document.getElementsByClassName('close-search')[0]
const searchBox = document.getElementsByClassName('search-dropdown')[0]

const stopScroll = function (e){
  if ((e.target === open || e.target.parentNode === open) && window.matchMedia('(max-width: 991px)').matches) {
    this.classList.add('no-scroll')
  }
}

const startScroll = function (e){
  if ((e.target === close || e.target.parentNode === close) && window.matchMedia('(max-width: 991px)').matches) {
    this.classList.remove('no-scroll')
    console.log(e.target)
  }
}

const openSearch = function (e) {
  this.parentNode.classList.toggle('open')
  if (searchBox.style.display === 'none'){
    searchBox.style.display = 'block'
  }
}

const closeSearch = function (e) {
  const close = this.getElementsByClassName('close-search')[0]
  if (e.target === close && this.parentNode.classList.contains('open')){
    this.parentNode.classList.remove('open')
  }
}


toggle.addEventListener('click', openSearch)
document.documentElement.addEventListener('click', stopScroll)
document.documentElement.addEventListener('click', startScroll)
toggle.addEventListener('touchStart', openSearch)
toggle.addEventListener('click', closeSearch)
toggle.addEventListener('touchStart', closeSearch)
