/* eslint-disable */

/*
 * shared/header/search
 */
import './assets/styles/style';
import './assets/scripts/script';


if ( process.env.NODE_ENV === 'development' ) {
  if (module.hot) {
    module.hot.accept()
  }
}
