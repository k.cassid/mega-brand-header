/* eslint-disable */

if ( process.env.NODE_ENV !== 'production' ) {
  if (module.hot) {
    module.hot.accept()
  }
}

import './assets/styles/style'

import '../tooltip/config'
